import java.io.*;

public class WriteToFile {
    public static void main(String[] args){
        EconomCar econCar1 = new EconomCar("Economic", "Daewoo Lanos",
                140, 11, 8000);
        EconomCar econCar2 = new EconomCar("Economic", "Dacia Logan",
                110, 9, 6000);
        StandardCar standCar1 = new StandardCar("Standard", "Mazda CX5",
                190, 8, 30000);
        StandardCar standCar2 = new StandardCar("Standard", "Skoda Octavia",
                175,10, 28000);
        PremiumCar premCar1 = new PremiumCar("Premium", "BMW 750",
                245,15, 50000);
        PremiumCar premCar2 = new PremiumCar("Premium", "Toyota Camry",
                230,13, 48000);
        PremiumCar premCar3 = new PremiumCar("Premium", "Tesla Model S",
                280,0, 70000);

        try {
            FileOutputStream fos = new FileOutputStream("fileCars.bin");
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(econCar1);
            oos.writeObject(econCar2);
            oos.writeObject(standCar1);
            oos.writeObject(standCar2);
            oos.writeObject(premCar1);
            oos.writeObject(premCar2);
            oos.writeObject(premCar3);

            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
