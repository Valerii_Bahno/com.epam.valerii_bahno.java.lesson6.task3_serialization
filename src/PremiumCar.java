class PremiumCar extends Car implements FindFastCar {

    public PremiumCar(String typeCar, String nameCar, int maxSpeed,
                      int consumptionFuel, int priceCar) {
        super(typeCar, nameCar, maxSpeed, consumptionFuel, priceCar);
    }

    @Override
    int sumPriceCar() {
        return getPriceCar();
    }

    public void CheeperCar(String name1, int price1,
                           String name2, int price2,
                           String name3, int price3) {
        int min = Math.min(price1, Math.min(price2, price3));
        if (min == price1) {
            System.out.println("The cheeper car is " + name1 + " - " + min);
        }
        else if (min == price2) {
            System.out.println("The cheeper car is " + name2 + " - " + min);
        }
        else {
            System.out.println("The cheeper car is " + name3 + " - " + min);
        }
    }

    @Override
    public void MaxSpeedCar(String name1, int speed1,
                            String name2, int speed2,
                            String name3, int speed3) {
        int max = Math.max(speed1, Math.max(speed2, speed3));
        if (max == speed1) {
            System.out.println("The faster car is " + name1 + " - " + max);
        }
        else if (max == speed2) {
            System.out.println("The faster car is " + name1 + " - " + max);
        }
        else {
            System.out.println("The faster car is " + name3 + " - " + max);
        }
    }
}

