import java.io.*;

public class ReadFromFile {
    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream("fileCars.bin");
            ObjectInputStream ois = new ObjectInputStream(fis);

            EconomCar econCar1 = (EconomCar) ois.readObject();
            EconomCar econCar2 = (EconomCar) ois.readObject();
            StandardCar standCar1 = (StandardCar) ois.readObject();
            StandardCar standCar2 = (StandardCar) ois.readObject();
            PremiumCar premCar1 = (PremiumCar) ois.readObject();
            PremiumCar premCar2 = (PremiumCar) ois.readObject();
            PremiumCar premCar3 = (PremiumCar) ois.readObject();

            System.out.println(econCar1);
            System.out.println(econCar2);
            System.out.println(standCar1);
            System.out.println(standCar2);
            System.out.println(premCar1);
            System.out.println(premCar2);
            System.out.println(premCar3);

            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
